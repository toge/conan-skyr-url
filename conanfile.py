from conans import ConanFile, CMake, tools
import shutil

class SkyrurlConan(ConanFile):
    name        = "skyr-url"
    version     = "1.12.0"
    license     = "BSL-1.0"
    author      = "toge.mail@gmail.com"
    url         = "https://bitbucket.org/toge/conan-skyr-url"
    homepage    = "https://github.com/cpp-netlib/url"
    description = "A C++ library that implements the WhatWG URL specification "
    topics      = ("url-parser", "url", "cpp", "cpp17", "whatwg-url")
    settings    = "os", "compiler", "build_type", "arch"
    generators  = "cmake"
    options     = {
        "without_exceptions"   : [True, False],
        "without_rtti"         : [True, False],
        "enable_json"          : [True, False],
        "enable_filesystem"    : [True, False],
    }
    default_options = {
        "without_exceptions"   : False,
        "without_rtti"         : False,
        "enable_json"          : True,
        "enable_filesystem"    : True,
    }

    def source(self):
        tools.get("https://github.com/cpp-netlib/url/archive/v{}.zip".format(self.version))
        shutil.move("url-{}".format(self.version), "url")
        tools.replace_in_file("url/CMakeLists.txt", "find_package(tl-expected CONFIG REQUIRED)",
                              '''
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()''' )

        tools.replace_in_file("url/CMakeLists.txt", "find_package(nlohmann_json CONFIG REQUIRED)", "")
        tools.replace_in_file("url/src/CMakeLists.txt", "tl::expected", "")
        tools.replace_in_file("url/CMakeLists.txt", "find_package(range-v3 CONFIG REQUIRED)", "")
        tools.replace_in_file("url/src/CMakeLists.txt", "range-v3", "")

    def build(self):
        cmake = CMake(self)
        cmake.definitions["skyr_BUILD_TESTS"]                 = False
        cmake.definitions["skyr_BUILD_WITHOUT_EXCEPTIONS"]    = self.options.without_exceptions
        cmake.definitions["skyr_BUILD_WITHOUT_RTTI"]          = self.options.without_rtti
        cmake.definitions["skyr_ENABLE_JSON_FUNCTIONS"]       = self.options.enable_json
        cmake.definitions["skyr_ENABLE_FILESYSTEM_FUNCTIONS"] = self.options.enable_filesystem
        cmake.configure(source_folder="url")
        cmake.build()

    def requirements(self):
        self.requires("tl-expected/[>= 1.0.0]")
        self.requires("range-v3/[>= 0.9.1]")
        if self.options.enable_json:
            self.requires("jsonformoderncpp/[>= 3.7.0]@vthiery/stable")

    def package(self):
        self.copy("*.hpp",   dst="include", src="url/include")
        self.copy("*.lib",   dst="lib",     keep_path=False)
        self.copy("*.dll",   dst="bin",     keep_path=False)
        self.copy("*.so",    dst="lib",     keep_path=False)
        self.copy("*.dylib", dst="lib",     keep_path=False)
        self.copy("*.a",     dst="lib",     keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["skyr-url"]
