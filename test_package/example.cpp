#include <iostream>

#include "skyr/url.hpp"

int main() {
    auto url = skyr::url("http://example.org/\xf0\x9f\x92\xa9");
    std::cout << url.pathname() << std::endl;
}
